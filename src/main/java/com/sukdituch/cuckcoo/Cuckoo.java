/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.cuckcoo;

/**
 *
 * @author focus
 */
public class Cuckoo {

    int N; // Size of table.
    final int numberOfTable = 2; // Number of tables, 2 is default.
    Node[][] T;
    int cntNode = 0;

    public Cuckoo(int size) {
        this.N = size;
        T = new Node[numberOfTable][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < numberOfTable; j++) {
                T[j][i] = null; // new Node(Integer.MIN_VALUE, null);
            }
        }
    }
    
    // Default constructor 
    public Cuckoo() {
        this.N = 29; // Prime number is recommend
        T = new Node[numberOfTable][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < numberOfTable; j++) {
                T[j][i] = null;
            }
        }
    }

    private int getHashIndex(int tableCase, int key) {
        switch (tableCase) {
            case 1 :  {
                return key % N;
            }
            case 2 :  {
                return (key / N) % N;
            }
        }
        return 0;
    }

    public void put(int key, String value) {
        //System.out.println("Now key is " + key + " value is " + value);
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            //System.out.println("Update value at key " + T[0][getHashIndex(1, key)].key);
            T[0][getHashIndex(1, key)].value = value;
            return;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            //System.out.println("Update value at key " + T[0][getHashIndex(1, key)].key);
            T[1][getHashIndex(2, key)].value = value;
            return;
        }

        int unposKey = 0;
        String unposValue = "";

        int i = 0;
        int cycleOccur = 0;
        do {
            //System.out.println("Now looking at table " + (i + 1));
            if (T[i][getHashIndex(i + 1, key)] == null) {
                T[i][getHashIndex(i + 1, key)] = new Node(key, value);
                cntNode++;
                //System.out.println("New node add !! " + key + " " + value + " at [" + (i + 1) + "," + getHashIndex(i + 1, key) + "]");
                //System.out.println("This round is end.");
                return;
            }
            Node temp = T[i][getHashIndex(i + 1, key)];
            //System.out.println("Duplicate index found !! which is " + T[i][getHashIndex(i + 1, key)].key + " " + T[i][getHashIndex(i + 1, key)].value + " at [" + (i + 1) + "," + getHashIndex(i + 1, key) + "]");
            T[i][getHashIndex(i + 1, key)] = new Node(key, value);
            //System.out.println("Replace with " + key + " " + value);
            key = temp.key;
            value = temp.value;
            //System.out.println("Holding " + key + " " + value);
            cycleOccur++;
            //System.out.println("Number of cycle " + cycleOccur);
            i = (i + 1) % 2;
            if (cycleOccur + 1 == N) {
                unposKey = T[i][getHashIndex(i + 1, key)].key;
                unposValue = T[i][getHashIndex(i + 1, key)].value;
            }
            //System.out.println("This round is end.");
        } while (cycleOccur < cntNode + 1);

        if (cycleOccur == N) {
            System.out.println("REHASH!!! Following input cause a cycle.");
            System.out.println(unposKey + " , " + unposValue + " not assign to array.\n");
        }

    }

    public String get(int key) {
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            return T[0][getHashIndex(1, key)].value;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            return T[1][getHashIndex(2, key)].value;
        }
        return null;
    }

    public void remove(int key) {
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            T[0][getHashIndex(1, key)] = null;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            T[1][getHashIndex(2, key)] = null;
        }
    }

    public void getTable() {
        for (int i = 1; i < numberOfTable + 1; i++) {
            for (int j = 0; j < N; j++) {
                if (T[i - 1][j] != null) {
                    System.out.println("[" + i + "," + j + "] = " + T[i - 1][j].key + " , " + T[i - 1][j].value);
                } else {
                    System.out.println("[" + i + "," + j + "] = is null");
                }
                System.out.println("");
            }
        }
    }

    public static void main(String[] args) {
        Cuckoo ck = new Cuckoo(11);
        ck.put(20, "20");
        ck.put(50, "50");
        ck.put(53, "53");
        ck.put(75, "75");
        ck.put(20, "20");
        ck.put(100, "100");
        ck.put(67, "67");
        ck.put(105, "105");
        ck.put(3, "3");
        ck.put(36, "36");
        ck.put(39, "39");
        // ck.put(6, "6"); // Cycle occur here
        ck.getTable();
    }

}

